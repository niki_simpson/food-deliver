module.exports = {
  content: [
    "./screens/**/*/{js,s,jsx,tsx}",
    "./pages/**/*/{js,s,jsx,tsx}",
    "./components/**/*/{js,s,jsx,tsx}",
  ],
};
